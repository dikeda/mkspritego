package mksprite

import (
	"appengine"
	"appengine/urlfetch"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	"net/http"
	"sync"
	//_ "expvar"
)

func init() {
	http.HandleFunc("/mksprite", handler)
}

type cache struct {
	sync.RWMutex
	imgMap map[string]image.Image
}

var imageCache = cache {
	imgMap : make(map[string]image.Image),
}

type imageInfo struct {
	url     string
	ch      chan image.Image
	img     image.Image
	success bool
}

func newImageInfo(url string) *imageInfo {
	return &imageInfo{
		url, make(chan image.Image), nil, false,
	}
}

func (this *imageInfo) fetch() {
	this.img, this.success = <-this.ch
}

func getIcon(c appengine.Context, url string) *imageInfo {

	imageInfo := newImageInfo(url)

	go func() {

		imageCache.RLock()
		if image, ok := imageCache.imgMap[url]; ok {
			imageInfo.ch <- image
			imageCache.RUnlock()
			return
		}
		imageCache.RUnlock()

		client := urlfetch.Client(c)

		resp, err := client.Get(url)
		if err != nil {
			close(imageInfo.ch)
			return
		}

		defer resp.Body.Close()

		c.Infof("HTTP GET returned status %v<br>", resp.Status)
		c.Infof("content length %v<br>", resp.ContentLength)
		//c.Infof("content header %v<br>", resp.Header)

		image, err := png.Decode(resp.Body)
		if err != nil {
			c.Errorf("error occurred on decode. %v", err)
			close(imageInfo.ch)
			return
		}
		
		imageCache.Lock()
		imageCache.imgMap[url] = image
		imageCache.Unlock()

		imageInfo.ch <- image
	}()

	return imageInfo
}

func handler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	c.Infof("Requested URL: %v", r.URL)

	imgs := [...]*imageInfo{
		getIcon(c, "http://a634.phobos.apple.com/us/r30/Purple6/v4/a4/a3/32/a4a33248-e62a-b43d-b437-b4b49474728a/mzl.qsafpfxt.png"),
		getIcon(c, "http://a1079.phobos.apple.com/us/r30/Purple6/v4/d4/45/29/d445295e-0181-a24e-a925-3042f51fa3d9/mzl.wzgvswnn.png"),
		getIcon(c, "http://a1853.phobos.apple.com/us/r30/Purple4/v4/65/60/39/656039c6-81b6-2617-09cd-2beba305cff0/mzl.lhyknuzy.png"),
		getIcon(c, "http://a934.phobos.apple.com/us/r30/Purple6/v4/7f/05/b5/7f05b56e-bc23-4a58-705d-face96a9f88d/mzl.zhnikjax.png"),
		getIcon(c, "http://a647.phobos.apple.com/us/r30/Purple/v4/5d/a7/0e/5da70e83-9e2c-d55e-57b4-3b029ff139cb/mzl.iwymvlrl.png"),
	}

	var totalHeight, maxWidth int
	for _, src := range imgs {

		src.fetch()
		c.Infof("img1.success %v<br>", src.success)
		if src.success == false {
			http.Error(w, "fetch error", http.StatusInternalServerError)
			return
		}
		b := src.img.Bounds()

		totalHeight += b.Size().Y
		if maxWidth < b.Size().X {
			maxWidth = b.Size().X
		}
	}

	rect := image.Rect(0, 0, maxWidth, totalHeight)
	dst := image.NewRGBA(rect)

	y := 0
	for _, src := range imgs {
		sr := src.img.Bounds()
		dp := image.Pt(0, y)
		r := sr.Sub(sr.Min).Add(dp)
		draw.Draw(dst, r, src.img, sr.Min, draw.Src)
		y += sr.Size().Y
	}

	w.Header().Add("Content-Type", "image/jpeg")
	jpeg.Encode(w, dst, nil)
}
